#!/bin/bash

if [[ $(id -u) -ne 0 ]]; then
   echo "The script need to be run as root." >&2
   exit 1
fi

USERNAME="solana"

if [[ ! -z $SUDO_USER ]]; then
    REAL_USER=$SUDO_USER
else
    REAL_USER=$USERNAME
fi

CMD_PREFIX="sudo -i -u $REAL_USER"

read -r -d '' USAGE << EOM
USAGE:
	$0 [OPTIONS] [VALIDATOR_IDENTITY] [VOTE_ACCOUNT]

OPTIONS:
	-v, --version <VERSION>	Solana CLI version to install
	-u, --url <URL> Solana Cluster RPC Endpoint URL
	-s, --slack-webhook <URL> Slack Webhook URL
	-d, --discord-webhook <URL> Discord Webhook URL
	-h, --help Show help

EOM

ARGS_COUNT=$#
echo "Number of arguments/options: $ARGS_COUNT"

ARGS=()

# Color Codes
RED='\e[0;31m'
BLUE='\e[0;34m'
YELLOW='\e[0;33m'
NC='\e[0m' # No Color

SLACK_WEBHOOK=""
DISCORD_WEBHOOK=""

for i in "$@"; do
	case $i in
		-v=*|--version=*)
			# ${string#substring} - Deletes shortest match of $substring from front of $string.
			VERSION="${i#*=}"
			# ${string#substring}
			# Deletes shortest match of $substring from front of $string
			shift
			;;
		-u=*|--url=*)
			URL="${i#*=}"
			shift
			;;
	  -s=*|--slack-webhook=*)
      SLACK_WEBHOOK="${i#*=}"
      shift
      ;;
    -d=*|--discord-webhook=*)
      DISCORD_WEBHOOK="${i#*=}"
      shift
      ;;
		-h|--help)
			SHOW_HELP=1
			exit 0
			;;
		--*=*)
			# Any other option
			shift
			;;
		*)
			# Unknown option, assume it's an argument
			ARGS+=("$i")
			shift
			;;
	esac
done

if [[ $SHOW_HELP -eq 1 ]]; then
        echo "$USAGE"
        exit 2
fi

if [[ $ARGS_COUNT -eq 0 ]]; then
        echo "$USAGE"
        exit 2
fi

# BEGIN

echo "Installing Solana environment and validator..."

echo -n "Are you sure you want to proceed? [Y/N]: "

read -n 1 ANS

echo $AND

if [[ ! $ANS =~ ^[Yy]$ ]]; then
        echo "Aborting..."
        exit 1
fi

echo -e "######################\n"

if [[ ${#ARGS[@]} -ne 2 ]]; then
	echo "The allowed arguments number is 2, ${#ARGS[@]} provided"
	exit 3
fi

VALIDATOR_IDENTITY=${ARGS[0]}
VOTE_ACCOUNT=${ARGS[1]}

if [[ -z $URL ]]; then
        echo -e "${YELLOW}WARNING:${NC} --url defaults to mainnet URL https://api.mainnet-beta.solana.com"
        URL="https://api.mainnet-beta.solana.com"
fi

echo "Total args: ${#ARGS[@]}"
echo "Args: ${ARGS[@]}"

echo -e "CLI Version: ${VERSION}"
echo -e "Cluster RPC Endpoint URL: ${URL}\n"

# Step 1 - Install Solana user/group and add to sudo group

echo -e "${BLUE}1) Step 1:${NC} Install Solana user/group and add to sudo group\n"

echo "Checking if solana user exists..."
if ! getent passwd ${USERNAME} >/dev/null; then
  echo "Adding solana user and group..."
  adduser --home /home/solana --shell /bin/bash solana
  usermod -aG sudo solana
else
  echo "User solana already exists..."
fi

echo -e "\n"

# Step 2 - Install Solana CLI

echo -e "${BLUE}2) Step 2:${NC} Install Solana CLI with version $VERSION\n"

if [[ -z $VERSION ]]; then
	echo -e "${RED}Error:${NC} Please choose desired Solana CLI version to install with --version option."
	exit 4
fi

if [[ $VERSION =~ ^(([0-9]+\.)?)(([0-9]+)\.)?(\*|[0-9]+)$ ]]; then
	echo -e "Valid Solana version format..."
else
	echo -e "${RED}Error:${NC} Invalid --version value. Format needed is: x.y.z (major.minor.patch)."
	exit 4
fi

SOLANA_NEEDS_INSTALL=0
SOLANA_NEEDS_UPGRADE=0

echo "Checking if Solana is installed or accesible in PATH..."

# Works only if running without sudo
# if ! [[ -x $(command -v solana) ]]; then
if [[ ! -x $($CMD_PREFIX which solana) ]]; then
	echo "Solana command does not exist or it's not executable."
	SOLANA_NEEDS_INSTALL=1
else
	echo "Solana command already exists."

	SOLANA_NEEDS_INSTALL=0

	echo "Checking installed Solana version version to match..."

	$CMD_PREFIX solana --version
	if [[ $($CMD_PREFIX solana --version) == *"$VERSION"* ]]; then
        	echo "$VERSION found - OK"
	else
        	echo "$VERSION not found - NOT OK"
		SOLANA_NEEDS_UPGRADE=1
	fi
fi

if [[ $SOLANA_NEEDS_INSTALL -eq 1 ]]; then
	echo -e "Installing Solana vesion $VERSION...\n"

	SOLANA_INSTALL_URL="https://release.solana.com/v$VERSION/install"

	sudo -u $USERNAME sh -c "$(curl -sSfL $SOLANA_INSTALL_URL)"
elif [[ $SOLANA_NEEDS_UPGRADE -eq 1 ]]; then
	$CMD_PREFIX solana-install init $VERSION
fi

echo "Final checking installed Solana version version to match..."

if [[ $($CMD_PREFIX solana --version) == *"$VERSION"* ]]; then
	echo "Final check $VERSION found - OK"
else
	echo "$VERSION not found - NOT OK"
	exit 5
fi

echo -e "\n"

# Step 3 - Connect to Solana Cluster RPC
echo -e "${BLUE}3) Step 3:${NC} Connecting to Solana Cluster RPC Endopoint...\n"

$CMD_PREFIX solana config set --url $URL --keypair "/home/solana/$VALIDATOR_IDENTITY"

echo -e "Checking the config..."
$CMD_PREFIX solana config get

echo -e "Checking the cluster version..."
$CMD_PREFIX solana cluster-version

echo -e "\n"

# Step 4 - System Tuning
echo -e "${BLUE}4) Step 3:${NC} System Tuning\n"

bash -c "cat >/etc/sysctl.d/20-solana-udp-buffers.conf <<EOF
# Increase UDP buffer size
net.core.rmem_default = 134217728
net.core.rmem_max = 134217728
net.core.wmem_default = 134217728
net.core.wmem_max = 134217728
EOF"

sysctl -p /etc/sysctl.d/20-solana-udp-buffers.conf

bash -c "cat >/etc/sysctl.d/20-solana-mmaps.conf <<EOF
# Increase memory mapped files limit
vm.max_map_count = 1000000

# Increase maximum number of open files
fs.file-max = 1024288
EOF"

sysctl -p /etc/sysctl.d/20-solana-mmaps.conf

bash -c "cat >/etc/security/limits.d/90-solana-nofiles.conf <<EOF
# Increase process file descriptor count limit
* - nofile 1000000
EOF"

bash -c "cat >>/etc/security/limits.conf <<EOF

* soft     nproc          762144
* hard     nproc          762144
* soft     nofile         762144
* hard     nofile         762144
EOF"

echo -e "Running systemctl daemon-reload..."
systemctl daemon-reload

echo -e "\n"

# Step 5 - Create validator file
echo -e "${BLUE}5) Step 5:${NC} Create Validator File\n"

if [[ ! -d "/home/$REAL_USER/bin" ]]; then
	$CMD_PREFIX mkdir -p "/home/$REAL_USER/bin"
fi

if [[ ! -f "/home/$REAL_USER/bin/validator.sh" ]]; then
        $CMD_PREFIX touch "/home/$REAL_USER/bin/validator.sh"
        $CMD_PREFIX chmod +x "/home/$REAL_USER/bin/validator.sh"
fi

$CMD_PREFIX cat <<EOF > /home/$REAL_USER/bin/validator.sh
#!/bin/bash
exec solana-validator \\
	--ledger /home/solana/validator-ledger \\
	--identity /home/solana/$VALIDATOR_IDENTITY \\
	--vote-account /home/solana/$VOTE_ACCOUNT \\
	--rpc-port 8899 \\
	--log /home/solana/solana-validator.log \\
	--private-rpc \\
	--dynamic-port-range 8000-8010 \\
	--snapshot-interval-slots 7000 \\
	--no-untrusted-rpc \\
	--trusted-validator CakcnaRDHka2gXyfbEd2d3xsvkJkqsLw2akB3zsN1D2S \\
	--trusted-validator DE1bawNcRJB9rVm3buyMVfr8mBEoyyu73NBovf2oXJsJ \\
	--trusted-validator GdnSyH3YtwcxFvQrVVJMm1JhTS4QVX7MFsX56uJLUfiZ \\
	--trusted-validator 7Np41oeYqPefeNQEHSv1UDhYrehxin3NStELsSKCT4K2 \\
	--entrypoint entrypoint.mainnet-beta.solana.com:8001 \\
	--entrypoint entrypoint2.mainnet-beta.solana.com:8001 \\
	--entrypoint entrypoint3.mainnet-beta.solana.com:8001 \\
	--entrypoint entrypoint4.mainnet-beta.solana.com:8001 \\
	--entrypoint entrypoint5.mainnet-beta.solana.com:8001 \\
	--limit-ledger-size \\
	--expected-genesis-hash \$EXPECTED_GENESIS_HASH
EOF

$CMD_PREFIX cat /home/$REAL_USER/bin/validator.sh

echo -e "\n"

# Step 6 - Create systemctl service file
echo -e "${BLUE}6) Step 6:${NC} Create systemctl service file\n"

cat <<EOF > /etc/systemd/system/solana.service
[Unit]
Description=Solana Mainnet Validator Node
After=network.target syslog.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=solana
LimitNOFILE=762144
LogRateLimitIntervalSec=0
Environment="PATH=/bin:/usr/bin:/home/solana/.local/share/solana/install/active_release/bin"
Environment="SOLANA_METRICS_CONFIG="host=https://metrics.solana.com:8086,db=mainnet-beta,u=mainnet-beta_write,p=password""
Environment="EXPECTED_SHRED_VERSION=8573"
Environment="EXPECTED_GENESIS_HASH=5eykt4UsFv8P8NJdTREpY1vzqKqZKvdpKuc147dw2N9d"
ExecStart=/home/solana/bin/validator.sh

[Install]
WantedBy=multi-user.target
EOF

cat /etc/systemd/system/solana.service

echo -e "\n"

echo -e "Running systemctl deamon-reload..."
systemctl daemon-reload

echo -e "\n"

# Step 7 - Setup log rotation for custom log file
echo -e "${BLUE}7) Step 7:${NC} Setup log rotation for custom log file\n"

$CMD_PREFIX cat > logrotate.sol <<EOF
/home/solana/solana-validator.log {
  rotate 7
  daily
  missingok
  postrotate
    systemctl kill -s USR1 solana.service
  endscript
}
EOF

cp logrotate.sol /etc/logrotate.d/solana
systemctl restart logrotate.service

echo -e "\n"

# Step 8 - Add env variables to .profile file
echo -e "${BLUE}8) Step 8:${NC} Add env variables to .profile file\n"

$CMD_PREFIX echo -e "\n" >> /home/solana/.profile
$CMD_PREFIX echo 'export SOLANA_METRICS_CONFIG="host=https://metrics.solana.com:8086,db=mainnet-beta,u=mainnet-beta_write,p=password"' >> /home/solana/.profile

if [[ ! -z $SLACK_WEBHOOK ]]; then
  $CMD_PREFIX echo "export SLACK_WEBHOOK=\"$SLACK_WEBHOOK\"" >> /home/solana/.profile
fi

if [[ ! -z $DISCORD_WEBHOOK ]]; then
  $CMD_PREFIX echo "export DISCORD_WEBHOOK=\"$DISCORD_WEBHOOK"\" >> /home/solana/.profile
fi

$CMD_PREFIX source /home/solana/.profile

echo -e "\n"

echo -e "DONE"

echo -e "\n"

# END
