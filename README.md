# Solana Validator Node Environment Installer

## Create solana user/group

```
adduser solana
```

Add `solana` user to sudo group:

```
usermod -aG sudo solana
```

## Copy `install_solana.sh` file to your /home/solana folder

Make sure the file is executable:
```
chmod +x install_solana.sh
```

## Run the script and usage

```
USAGE:
install_solana.sh [OPTIONS] [VALIDATOR_IDENTITY] [VOTE_ACCOUNT]

OPTIONS:
-v, --version <VERSION>	Solana CLI version to install
-u, --url <URL> Solana Cluster RPC Endpoint URL
-s, --slack-webhook <URL> Slack Webhook URL
-d, --discord-webhook <URL> Discord Webhook URL
-h, --help Show help
```

Run the bash script as root or with `sudo` if you already created the solana user/group. Use the following arguments and options. The needed Solana commands will be run as `solana` user: `sudo -u solana <command>`.
Make sure to change with your own keypair filenames and Solana CLI version.  

* The validator identity keypair and vote address keypair filenames should exist in `/home/solana` folder
* Example Solana CLI version: 1.8.5

```
bash install_solana.sh --version=<version> --slack-webhook=<URL> --discord-webhook=<URL> mainnet-validator-keypair.json mainnet-vote-account-keypair.json
```

OR

```
bash install_solana.sh -v=<version> -s=<URL> -d=<URL> mainnet-validator-keypair.json mainnet-vote-account-keypair.json
```
